if test (uname) != "Darwin"
	function open --wraps=xdg-open
		for arg in $argv
			xdg-open $arg &
		end
	end
end
