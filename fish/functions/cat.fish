# The "bat" program is actually "batcat" on Ubuntu, for reasons.
if type -f -q bat
	function cat --wraps=bat
		bat $argv
	end
else if type -f -q batcat
	function cat --wraps=batcat
		batcat $argv
	end
end
