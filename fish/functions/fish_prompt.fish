function fish_prompt
	# Get the current directory in terms of "~"
	set -l pwd (string replace -r '^'"$HOME"'($|/)' '~$1' -- $PWD)

	# Display the current directory.
	echo -en "\n"(set_color normal)"┌─["(set_color green)$pwd(set_color normal)"]"

	# Display the Git branch if we're in a repo
	if test -n "$__git_branch"
		echo -n "─["(set_color red)$__git_branch(set_color normal)"]"
	end

	# If we're present via SSH, indicate it.
	if set -q SSH_CLIENT; or set -q SSH_TTY; or set -q SSH_CONNECTION
		echo -n "─["(set_color cyan)"SSH"(set_color normal)"]"
	end

	echo -e "\n└─["(set_color purple)(hostname -s)(set_color normal)"]-> "
end

# Have a function that updates the Git repo every time we change dirs
function __get_git_branch --on-variable PWD --on-event GIT_COMMAND
	set -g __git_suppress_emit 1

	# This is ugly as hell but I'm not sure if there is another way
	# to both assign a value and test at the same time. It's also
	# not helpful that Fish is difficult to Google for.
	set -l branch (git branch --show-current 2> /dev/null)
	if test -n "$branch"
		set -g __git_branch "$branch"
	else
		set -l tag (git describe --exact-match 2> /dev/null)
		if test -n "$tag"
			set -g __git_branch "$tag"
		else
			set -l rev (git rev-parse --short HEAD 2> /dev/null)
			if test -n "$rev"
				set -g __git_branch "$rev"
			else
				set -g __git_branch ""
			end
		end
	end


	set -ge __git_suppress_emit
end
