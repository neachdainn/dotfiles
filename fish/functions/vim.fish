if type -f -q nvim
	function vim --wraps=nvim
		nvim $argv
	end
end
