function fish_user_key_bindings
	# Apparently my fingers don't play well when doing ctrl+z and then up.
	bind \e\[1\;5A up-or-search
	bind \e\[1\;5B down-or-search
end
