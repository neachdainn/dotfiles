function cp --wraps=cp
	command cp -iva $argv
end
