if type -f -q exa
	function ls --wraps=exa
		# Ubuntu 22.04 disables the `git` feature.
		exa --group --header $argv
	end
else if type -f -q eza
	function ls --wraps=eza
		eza --group --header --git $argv
	end
else
	function ls --wraps=ls
		command ls --color=auto $argv
	end
end
