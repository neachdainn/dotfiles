function weather -d "Displays the forcast."
	argparse -X 1 "h/help" "m/metric" -- $argv;
	or return $status

	set -l location (string replace -a " " "+" $argv[1])
	set -l units "u"

	if test -n "$_flag_help"
		set location ":help"
	end

	if test -n "$_flag_metric"
		set units "m"
	end

	curl "http://wttr.in/$location?$units"
end
