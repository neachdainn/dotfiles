if type -f -q htop
	function top --wraps=htop
		htop $argv
	end
end
