function rm --wrap=rm
	command rm -Iv $argv
end
