# Wrapper for Git to power the prompt.
function git --wraps=git
	command git $argv

	# Only emit if we're the first in the stack.
	if not set -q __git_suppress_emit
		emit GIT_COMMAND
	end
end
