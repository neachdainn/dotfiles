# The "bat" program is actually "batcat" on Ubuntu.
if type -f -q bat
	function man --wraps=man
		set -lx MANPAGER "sh -c 'col -bx | bat -l man -p'"
		command man $argv
	end
else if type -f -q batcat
	function man --wraps=man
		set -lx MANPAGER "sh -c 'col -bx | batcat -l man -p'"
		command man $argv
	end
end
