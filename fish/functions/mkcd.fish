function mkcd --description "Makes a new directory and moves into it"
	if test (count $argv) -ne 1
		echo "Expected one argument"
		return 1
	end

	mkdir -p $argv[1]; and cd $argv[1]
end
