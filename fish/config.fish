# Items for interactive login shells
if status --is-interactive; and status --is-login
	# The present directory is not implicit before (I believe) Fish 3.1
	set -x CDPATH "." "$HOME" "$HOME/code" $CDPATH

	# GPG has issues with whatever Kitty/fish setup I have.
	set -x GPG_TTY (tty)
end

# Login shell items
if status --is-login
	# On macOS, we want to use the GNU binaries over the OSX ones. Also, for some reason, Fish on my
	# macOS has a totally empty path variable.
	if test (uname) = "Darwin"
		set -x PATH "/Library/TeX/texbin" "/usr/local/opt/gnu-units/libexec/gnubin" "/usr/local/opt/coreutils/libexec/gnubin" "/usr/local/bin" "/usr/bin" "/bin" 2> /dev/null
	end

	# Set the path to include items installed in the home directory
	set -x PATH "$HOME/bin" "$HOME/.local/bin" "$HOME/.cargo/bin" $PATH 2> /dev/null

	# Default to Neovim if available, otherwise just Vim. This might not be necessary - it might be
	# sufficient to set it to "vim" and let the alias do the work.
	if type -f -q "nvim"
		set -x EDITOR "nvim"
	else
		set -x EDITOR "vim"
	end

	# Pesky programs
	set -x WEECHAT_HOME "$HOME/.config/weechat"
end
