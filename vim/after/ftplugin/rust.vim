" Use our own tab styles
let g:rust_recommended_style=0

" Utilize the conceal
let g:rust_conceal=1

" Mark where the column limit is and make it a more reasonable modern default
setlocal colorcolumn=101
setlocal textwidth=100
