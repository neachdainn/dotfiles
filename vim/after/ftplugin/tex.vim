" Turn on the spell checker
setlocal spell

" We also want to be able to see all the words even if that means line wrapping. We also don't like
" hard linebreaks.
setlocal wrap
setlocal linebreak

" Make moving across multiline lines easier
nnoremap <buffer> j gj
nnoremap <buffer> k gk

" Add a wordcount button
map <buffer> <F3> :w !detex \| wc -w<CR>

" Use the conceal feature to make things easier to read
if has("conceal")
	setlocal cole=2
	let g:tex_conceal = "adgm"
endif
