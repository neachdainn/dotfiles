" Bibtex file
setlocal shiftwidth=3
setlocal softtabstop=3
setlocal tabstop=3

setlocal wrap
setlocal linebreak

setlocal spell

setlocal foldmethod=marker
