setlocal noexpandtab
setlocal sw=4 ts=4

" Mark where the column limit is and make it a more reasonable modern default
setlocal colorcolumn=101
setlocal textwidth=100
