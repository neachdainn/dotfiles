" Turn on spelling
setlocal spell

" Make moving across multiline lines easier
nnoremap <buffer> j gj
nnoremap <buffer> k gk

" Make things flow better in sent emails
setlocal tw=0
setlocal wrapmargin=0

" Make things look better while editing
setlocal wrap
setlocal linebreak

if has('linebreak') && has('patch-7.4-338')
	setlocal nobreakindent
endif

" This is a hack to softwrap at a specific column. I do not like this. I do not like this at all.
" 120 was chosen as a length that won't wrap any of the headers
setlocal columns=120
setlocal colorcolumn=121
augroup mail_editor
	autocmd!
	autocmd VimResized * if (&columns > 120) | set columns=120 | endif
augroup END
