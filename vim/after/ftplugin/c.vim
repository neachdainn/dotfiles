" C{++} specific abbreviations
abb <buffer>  #i #include
abb <buffer>  #d #define
abb <buffer> unsinged unsigned

" Map <F5> to the make command for faster compiling
map <buffer> <F5> :make<Enter>

" Mark where the column limit is and make it a more reasonable modern default
setlocal colorcolumn=101
setlocal textwidth=100

" I could set the style here to match the lab's, and I probably should because that's where most of
" my C++ programming happens, but I would rather keep this as my personal settings and manually
" change plus use clang-format for the lab.
"setlocal expandtab
"setlocal shiftwidth=2
"setlocal tabstop=2
"setlocal softtabstop=2
