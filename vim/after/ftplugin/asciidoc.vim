" Turn on the spell checker
setlocal spell

" We also want to be able to see all the words even if that means line wrapping
setlocal wrap
setlocal linebreak

" Make moving across multiline lines easier
nnoremap <buffer> j gj
nnoremap <buffer> k gk
