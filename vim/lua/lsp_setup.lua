local lsp_config = require('lspconfig')

-- Set the diagnostics to be minimally invasive
vim.lsp.handlers["textDocument/publishDiagnostics"] = vim.lsp.with(
	vim.lsp.diagnostic.on_publish_diagnostics, {
		-- Rust-analyzer's text is much too large
		virtual_text = false,

		-- We only care about the most severe thing on the line
		severity_sort = true,
	}
)

-- Don't use symbols in the gutter, color the line number
vim.fn.sign_define("DiagnosticSignError", {text = "", numhl = "DiagnosticSignError"})
vim.fn.sign_define("DiagnosticSignWarn", {text = "", numhl = "DiagnosticSignWarn"})
vim.fn.sign_define("DiagnosticSignInfo", {text = "", numhl = "DiagnosticSignInfo"})
vim.fn.sign_define("DiagnosticSignHint", {text = "", numhl = "DiagnosticSignHint"})

--- Rust-analyzer specific setup
local rust_on_attach = function(client, bufnr)
	local function buf_set_keymap(...) vim.api.nvim_buf_set_keymap(bufnr, ...) end
	local function buf_set_option(...) vim.api.nvim_buf_set_option(bufnr, ...) end

	-- Enable a form of tab-completion
	buf_set_option('omnifunc', 'v:lua.vim.lsp.omnifunc')

	-- Add mappings for LSP functionality
	local opts = { noremap=true, silent=true }
	buf_set_keymap('n', '[e', '<cmd>lua vim.diagnostic.goto_prev()<cr>', opts)
	buf_set_keymap('n', ']e', '<cmd>lua vim.diagnostic.goto_next()<cr>', opts)
	buf_set_keymap('n', 'gH', '<cmd>lua vim.lsp.buf.hover()<cr>', opts)
	buf_set_keymap('n', 'gD', '<cmd>lua vim.lsp.buf.definition()<cr>', opts)
	buf_set_keymap('n', 'gS', '<cmd>lua vim.lsp.buf.signature_help()<cr>', opts)
	buf_set_keymap('n', 'ga', '<cmd>lua vim.lsp.buf.code_action()<cr>', opts)

	buf_set_keymap('i', '<c-x><c-e>', '<cmd>lua vim.lsp.buf.signature_help()<cr>', opts)

	-- Symantic highlighting is not yet better than a good syntax file
	client.server_capabilities.semanticTokensProvider = nil
end
lsp_config.rust_analyzer.setup({
	on_attach=rust_on_attach,
	settings = {
		["rust_analyzer"] = {
			checkOnSave = { command = "clippy" },
		},
	},
})
